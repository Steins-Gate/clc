# Unsupervised Lifelong Learning with Curricula (ULLC) #

This demo demonstrates the experiments performed on the dataset D6
and could be compiled in an ''one-stop'' fashion, which includes
1) data generation; 
2) model training; 
3) model evaluation; and 
4) accuracy report.   
 
The complete code repository will be published along with the manuscript.   

## Requirements ##
Please see details in `requirements.txt` file   
* numpy  
* pytorch  
* torchvision   
* scipy  
* sklearn   
* tqdm  
* pickle  
* prettytable   

## Run ##

For Python 3.X users:
```
python main.py --gamma --lr --batchsize --nepoch --tau
```
`--gamma`: a positive parameter controlling the scales of two loss terms. Default: 3   
`--lr`: learning rate. Default: 1e-4   
`--batchsize`: batch size. Default: 16   
`--nepoch`: number of training epochs. Note, this parameter
differs from the '# of Epochs' reported in the experiments which 
indeed indicates stochastic updating steps. Default: 4     
`--tau`: a parameter for updating weight factors. Default: .005   


## Results ##

### Forward Transfer Rate    

| After handling | T_0 (*)    | T_1  | T_2  | T_3  | T_4  | T_5  | T_6  | T_7  | T_8  | T_9  |
|----------------|------|------|------|------|------|------|------|------|------|------|
| ACC(%) on T_10 | 59.7 | 54.9 | 60.9 | 63.3 | 70.1 | 72.9 | 75.4 | 83.7 | 85.6 | 88.9 |   
   
(*) Note, only **T_0** is labeled; All other tasks **T_1**, ..., **T_10** do NOT have any labeled instance.   
### Evolution of Weight Factors    
![alt text][FIG]  

[FIG]: /plots/alpha_results.jpg "FIG"