import os
os.environ['CUDA_LAUNCH_BLOCKING'] = "0"
import numpy as np
import torch
import torch.optim as optim
import torch.utils.data
from torch.nn.parameter import Parameter
from torchvision import datasets
from torchvision import transforms
import tqdm
import data_loader
from model import ETAN
from synthetic_data import get_dataset
import argparse
from prettytable import PrettyTable
import pickle
import warnings
import random
warnings.filterwarnings("ignore")
torch.manual_seed(10)
torch.cuda.manual_seed(10)
np.random.seed(23)
random.seed(23)

parser = argparse.ArgumentParser()
parser.add_argument('--gamma', type=float, default=3) #3
parser.add_argument('--lr', type=float, default=1e-4)
parser.add_argument('--batchsize', type=int, default=1000)
parser.add_argument('--cuda', type=int, default=0)
parser.add_argument('--nepoch', type=int, default=4)
parser.add_argument('--source', type=str, default='mnist')
parser.add_argument('--target', type=str, default='mnist_rb')
parser.add_argument('--model_path', type=str, default='models')
parser.add_argument('--result_path', type=str, default='result/result.csv')
parser.add_argument('--tau', type=float, default=-.005)
args = parser.parse_args()
DEPTH = 10
DEVICE = torch.device('cuda:{}'.format(args.cuda)) if torch.cuda.is_available() else torch.device('cpu')
tau = Parameter(torch.tensor(args.tau), requires_grad=False).to(DEVICE)
llambda = Parameter(torch.tensor(args.gamma), requires_grad=False).to(DEVICE)



gaussian_matrix = np.random.normal(0, 1, (10, 10))
loader_src = get_dataset(1000, 11, 2, map=None, drawn='src')
loader_tar_1  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=1)
loader_tar_2  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=2)
loader_tar_3  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=3)
loader_tar_4  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=4)
loader_tar_5  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=5)
loader_tar_6  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=6)
loader_tar_7  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=7)
loader_tar_8  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=8)
loader_tar_9  = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=9)
loader_tar_10 = get_dataset(1000, 11, 2, map=gaussian_matrix, drawn=10)

LabelLoss = []
DomainLoss = []

def test(model, dataset_name, epoch):
    alpha = 0
    dataloader = dataset_name
    model.eval()
    n_correct = 0
    with torch.no_grad():
        for _, dict_tar in enumerate(dataloader):
            # print(dict_tar)
            t_img, t_label = dict_tar['features'], dict_tar['label']
            # print(t_img)
            t_img, t_label = t_img.to(DEVICE).squeeze(), t_label.to(DEVICE).squeeze()
            class_output, _ = model(input_data=t_img.view(-1,11), input_label=t_label, alpha=alpha)
            prob, pred = torch.max(class_output.data, 1)
            n_correct += (pred == t_label.long()).sum().item()

    acc = float(n_correct) / len(dataloader.dataset) * 100
    return acc


def train(model, optimizer, dataloader_src, dataloader_tar):
    loss_class = torch.nn.CrossEntropyLoss()
    # m = torch.nn.LogSoftmax()
    best_acc = -float('inf')
    len_dataloader = min(len(dataloader_src), len(dataloader_tar))
    for epoch in range(args.nepoch):
        model.train()
        i = 1
        for (data_src, data_tar) in tqdm.tqdm(zip(enumerate(dataloader_src), enumerate(dataloader_tar)),
                                              total=len_dataloader, leave=False):
            _, dict_src = data_src
            x_src = dict_src['features']
            y_src = dict_src['label']
            _, dict_tar = data_tar
            x_tar = dict_tar['features']
            x_src, y_src, x_tar = x_src.to(DEVICE).squeeze(), y_src.to(DEVICE).squeeze(), x_tar.to(DEVICE).squeeze()
            p = float(i + epoch * len_dataloader) / args.nepoch / len_dataloader
            alpha = 2. / (1. + np.exp(-10 * p)) - 1

            class_output, err_s_domain = model(input_data=x_src, input_label=y_src, alpha=alpha)
            err_s_label = loss_class(class_output, y_src)
            _, err_t_domain = model(input_data=x_tar, input_label=y_src, alpha=alpha, source=False)
            err_domain = err_t_domain + err_s_domain
            global LabelLoss, DomainLoss
            LabelLoss.append(err_s_label), DomainLoss.append(err_domain)
            err = err_s_label + args.gamma * err_domain
            optimizer.zero_grad()
            err.backward()
            optimizer.step()
            i += 1
        item_pr = 'Epoch: [{}/{}], supervision_loss: {:.4f}, adversarial_loss: {:.4f}'.format(
            epoch+1, args.nepoch, err_s_label.item(), err_domain.item())
        print(item_pr)

        # predict
        acc_src = test(model, dataloader_src, epoch)
        acc_tar = test(model, dataloader_tar, epoch)
        test_info = 'Training acc: {:.4f} (on current task, NOT on T_10)'.format(acc_src)
        if best_acc < acc_tar:
            best_acc = acc_tar
            if not os.path.exists(args.model_path):
                os.makedirs(args.model_path)
            torch.save(model, '{}/mnist_rb'.format(args.model_path))
    # print(test_info)
    print()
    print()
    return best_acc

if __name__ == '__main__':
    resultTable = PrettyTable(['After handling .. ', 'Test Acc (%) on T_10'])
    for task in range(10):
        model = ETAN(DEVICE, depth=DEPTH, inputDim=11).to(DEVICE)
        optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=5e-4)
        if task == 0:
            print('Now, learning the source S:')
            acc = train(model, optimizer, loader_src, eval("loader_tar_"+str(10)))
            resultTable.add_row(['S', round(acc, 2)])
        else:
            print('Now, learning the {0}th target T_{0}:'.format(str(task)))
            if task > 4: model.tau, model.llambda = tau, llambda
            acc = train(model, optimizer, eval("loader_tar_"+str(task)), eval("loader_tar_"+str(10)))
            resultTable.add_row(['T_'+str(task), round(acc, 2)])
    print(resultTable)


