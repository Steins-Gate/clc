import torch
import torch.nn as nn
import adv_layer
import torch.nn.functional as F
from torch.nn import Linear, ReLU
from torch.nn.parameter import Parameter


class FeatureExtractor(nn.Module):
    def __init__(self):
        super(FeatureExtractor, self).__init__()
        feature = nn.Sequential()
        feature.add_module('f_conv1', nn.Conv2d(3, 64, kernel_size=5))
        feature.add_module('f_bn1', nn.BatchNorm2d(64))
        feature.add_module('f_pool1', nn.MaxPool2d(2))
        feature.add_module('f_relu1', nn.ReLU(True))
        feature.add_module('f_conv2', nn.Conv2d(64, 50, kernel_size=5))
        feature.add_module('f_bn2', nn.BatchNorm2d(50))
        feature.add_module('f_drop1', nn.Dropout2d())
        feature.add_module('f_pool2', nn.MaxPool2d(2))
        feature.add_module('f_relu2', nn.ReLU(True))
        self.feature = feature

    def forward(self, x):
        return self.feature(x)


class ETANFeatureExtractor(nn.Module):
    def __init__(self, inputDim, width):
        super(ETANFeatureExtractor, self).__init__()
        self.fc1 = Linear(inputDim, width)
        self.fc2 = Linear(width, width)
        self.fc3 = Linear(width, width)
        self.fc4 = Linear(width, width)
        self.fc5 = Linear(width, width)
        self.fc6 = Linear(width, width)
        self.fc7 = Linear(width, width)
        self.fc8 = Linear(width, width)
        self.fc9 = Linear(width, width)
        self.act1 = ReLU()
        self.act2 = ReLU()
        self.act3 = ReLU()
        self.act4 = ReLU()
        self.act5 = ReLU()
        self.act6 = ReLU()
        self.act7 = ReLU()
        self.act8 = ReLU()
        self.act9 = ReLU()

    def forward(self, x):
        x1 = self.fc1(x)
        output1 = self.act1(x1)
        x2 = self.fc2(output1)
        output2 = self.act1(x2)
        x3 = self.fc3(output2)
        output3 = self.act1(x3)
        x4 = self.fc4(output3)
        output4 = self.act1(x4)
        x5 = self.fc5(output4)
        output5 = self.act5(x5)
        x6 = self.fc6(output5)
        output6 = self.act6(x6)
        x7 = self.fc7(output6)
        output7 = self.act7(x7)
        x8 = self.fc8(output7)
        output8 = self.act8(x8)
        x9 = self.fc9(output8)
        output9 = self.act9(x9)
        return [output1, output2, output3, output4, output5,
                output6, output7, output8, output9]



class Classifier(nn.Module):
    def __init__(self, depth, width, inputDim):
        super(Classifier, self).__init__()
        self.class_classifier = nn.Sequential()
        self.class_classifier.add_module('c_fc1', nn.Linear(inputDim, width))
        self.class_classifier.add_module('c_relu1', nn.ReLU(True))
        for k in range(depth-1):
            k = k+2
            self.class_classifier.add_module('c_fc'+str(k), nn.Linear(width, width))
            self.class_classifier.add_module('c_relu'+str(k), nn.ReLU(True))
        self.class_classifier.add_module('c_fc3'+str(depth+2), nn.Linear(width, 2))


    def forward(self, x):
        return self.class_classifier(x)

class ETANClassifier(nn.Module):
    def __init__(self, width, inputDim):
        super(ETANClassifier, self).__init__()
        self.fc1 = Linear(inputDim, width)
        self.fc2 = Linear(width, 2)
        self.act1 = ReLU()
        self.act2 = ReLU()


    def forward(self, x):
        x1 = self.fc1(x)
        output1 = self.act1(x1)
        x2 = self.fc2(output1)
        output2 = self.act2(x2)
        return output2



class ETAN(nn.Module):

    def __init__(self, device, depth=11, width=100, inputDim = 11):
        super(ETAN, self).__init__()
        self.device, self.depth = device, depth -1
        self.alpha = Parameter(torch.Tensor(self.depth).fill_(1 / (self.depth + 1)),
                               requires_grad=False).to(self.device)
        self.feature = ETANFeatureExtractor(inputDim=inputDim, width=width)
        self.classifier_0 = ETANClassifier(width=width, inputDim=width)
        self.classifier_1 = ETANClassifier(width=width, inputDim=width)
        self.classifier_2 = ETANClassifier(width=width, inputDim=width)
        self.classifier_3 = ETANClassifier(width=width, inputDim=width)
        self.classifier_4 = ETANClassifier(width=width, inputDim=width)
        self.classifier_5 = ETANClassifier(width=width, inputDim=width)
        self.classifier_6 = ETANClassifier(width=width, inputDim=width)
        self.classifier_7 = ETANClassifier(width=width, inputDim=width)
        self.classifier_8 = ETANClassifier(width=width, inputDim=width)
        self.domain_classifier_0 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.domain_classifier_1 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.domain_classifier_2 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.domain_classifier_3 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.domain_classifier_4 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.domain_classifier_5 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.domain_classifier_6 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.domain_classifier_7 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.domain_classifier_8 = adv_layer.Discriminator(input_dim=width, hidden_dim=width)
        self.loss_fn = nn.BCELoss().to(self.device)
        self.criterion = nn.CrossEntropyLoss().to(self.device)
        self.sub_losses = Parameter(torch.Tensor(self.depth).fill_(0),
                               requires_grad=False).to(self.device)
        self.b = Parameter(torch.tensor(.99), requires_grad=False).to(self.device)
        self.s = Parameter(torch.tensor(.2), requires_grad=False).to(self.device)
        self.tau = Parameter(torch.tensor(-.002), requires_grad=False).to(self.device)
        self.low_val = Parameter(torch.tensor(1e-8), requires_grad=False).to(self.device)
        self.llambda = Parameter(torch.tensor(5), requires_grad=False).to(self.device)


    def forward(self, input_data, input_label, alpha=1, source=True):
        # input_data = input_data.expand(len(input_data), 3, 28, 28)
        features = self.feature(input_data)
        label = input_label
        weight = self.alpha.clone()
        # feature = feature.view(-1, 50 * 4 * 4)
        class_output_0 = self.classifier_0(features[0])
        class_output_1 = self.classifier_1(features[1])
        class_output_2 = self.classifier_2(features[2])
        class_output_3 = self.classifier_3(features[3])
        class_output_4 = self.classifier_4(features[4])
        class_output_5 = self.classifier_5(features[5])
        class_output_6 = self.classifier_6(features[6])
        class_output_7 = self.classifier_7(features[7])
        class_output_8 = self.classifier_8(features[8])
        class_output = torch.add(
            torch.add(
                torch.add(
                    torch.add(
                        torch.add(
                            torch.add(
                                torch.add(
                                    torch.add(
                                        torch.mul(weight[0], class_output_0),
                                        torch.mul(weight[1], class_output_1)
                                    ),
                                         torch.mul(weight[2], class_output_2)
                                ),
                                         torch.mul(weight[3], class_output_3)
                            ),
                                         torch.mul(weight[4], class_output_4)
                        ),
                                         torch.mul(weight[5], class_output_5)
                    ),
                                         torch.mul(weight[6], class_output_6)
                ),
                                         torch.mul(weight[7], class_output_7)
            ),
                                         torch.mul(weight[8], class_output_8)
        )

        if source:
            domain_label = torch.ones(len(features[0])).long().to(self.device)
        else:
            domain_label = torch.zeros(len(features[0])).long().to(self.device)

        domain_pred_0 = self.domain_classifier_0(
            adv_layer.ReverseLayerF.apply(features[0], alpha)
        )
        domain_pred_1 = self.domain_classifier_1(
            adv_layer.ReverseLayerF.apply(features[1], alpha)
        )
        domain_pred_2 = self.domain_classifier_1(
            adv_layer.ReverseLayerF.apply(features[2], alpha)
        )
        domain_pred_3 = self.domain_classifier_1(
            adv_layer.ReverseLayerF.apply(features[3], alpha)
        )
        domain_pred_4 = self.domain_classifier_1(
            adv_layer.ReverseLayerF.apply(features[4], alpha)
        )
        domain_pred_5 = self.domain_classifier_1(
            adv_layer.ReverseLayerF.apply(features[5], alpha)
        )
        domain_pred_6 = self.domain_classifier_1(
            adv_layer.ReverseLayerF.apply(features[6], alpha)
        )
        domain_pred_7 = self.domain_classifier_1(
            adv_layer.ReverseLayerF.apply(features[7], alpha)
        )
        domain_pred_8 = self.domain_classifier_1(
            adv_layer.ReverseLayerF.apply(features[8], alpha)
        )
        domain_pred = torch.add(
            torch.add(
                torch.add(
                    torch.add(
                        torch.add(
                            torch.add(
                                torch.add(
                                    torch.add(
                                        torch.mul(weight[0], domain_pred_0),
                                        torch.mul(weight[1], domain_pred_1)
                                    ),
                                    torch.mul(weight[2], domain_pred_2)
                                ),
                                torch.mul(weight[3], domain_pred_3)
                            ),
                            torch.mul(weight[4], domain_pred_4)
                        ),
                        torch.mul(weight[5], domain_pred_5)
                    ),
                    torch.mul(weight[6], domain_pred_6)
                ),
                torch.mul(weight[7], domain_pred_7)
            ),
            torch.mul(weight[8], domain_pred_8)
        )
        loss_adv = self.loss_fn(domain_pred, domain_label.float())

        for i in range(9):
            sub_loss = self.criterion(eval("class_output_"+str(i)), label)
            sub_loss_domain = self.loss_fn(eval("domain_pred_"+str(i)), domain_label.float())
            self.sub_losses[i] += torch.add(sub_loss, torch.mul(self.llambda, sub_loss_domain))

        for i in range(9):
            '''
                        1st hedgying method, updating \alpha pessively.
            '''
            # self.alpha[i] *= torch.pow(self.b, self.sub_losses[i])
            # self.alpha[i] = torch.max(self.alpha[i], self.s / self.depth)
            '''
                        2nd hedgying method, same as reported in the manuscript, 
                        leading to aggressive updates.
            '''
            self.alpha[i] = torch.exp(torch.mul(self.tau, self.sub_losses[i]))
            self.alpha[i] = torch.max(self.alpha[i], self.low_val)
        z_t = torch.sum(self.alpha)
        self.alpha = Parameter(self.alpha / z_t, requires_grad=False).to(self.device)


        return class_output, loss_adv

