import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn.datasets import make_blobs, make_moons, make_classification
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.manifold import TSNE
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from random import randint
from math import ceil

RSeed = 0
np.random.seed(RSeed)
# n_samples = 320
# outliers_fraction = 0.1
# n_outliers = int(outliers_fraction * n_samples)
# n_inliers = n_samples - n_outliers


def syndata(numSamples, numFeatures, numBlobs, map, domain = 'src'):
    blobs_params = dict(random_state=RSeed, n_samples=numSamples, n_features=numFeatures)
    # X_blob, y_blob = make_blobs(centers=numBlobs, **blobs_params)
    X_blob, y_blob = make_classification(**blobs_params,
                                         n_informative=numFeatures,
                                         n_redundant=0,
                                         n_classes =2,
                                         n_clusters_per_class=2)
    y_blob = y_blob.reshape(-1, 1)
    if domain == 'src':
        # print('Source Task')
        return np.hstack((X_blob, y_blob))
    else:
        # print('Target Task, Shift = ' + str(domain))
        # gaussian_matrix = np.random.normal(0, 1, (domain, domain))
        X_blob_sel = np.dot(X_blob[:, 0:ceil(domain/2)], map[:ceil(domain/2), :ceil(domain/2)]) + domain/2
        X_blob[:, 0:ceil(domain/2)] = X_blob_sel
        # X_blob = X_blob + domain
        return np.hstack((X_blob, y_blob))

class ToTensor(object):
    def __call__(self, sample):
        features, label = sample['features'], sample['label']
        # image = image.transpose((2, 0, 1))
        return {'features': torch.from_numpy(features).float(),
                'label': torch.from_numpy(label).long()}


class SynDataGenerator(Dataset):

    def __init__(self, numSamples = 100, numFeatures = 10, numBlobs = 3, map=None, transform=None, drawn='src'):
        self.dataset = syndata(numSamples, numFeatures, numBlobs, map, domain = drawn)# get a data matrix with labels in the last column
        self.transform = transform
    def __len__(self):
        return len(self.dataset) # the above data matrix
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        irow = self.dataset[idx]
        # print(irow)
        features = np.array(irow[0:-1]).astype('double').reshape(1,-1)
        label = np.array(irow[-1]).astype('float').flatten()
        sample = {'features': features, 'label': label}# finally get a dictionary with features and labels
        if self.transform:
            sample = self.transform(sample)
        return sample


def get_dataset(n_samples, n_features, n_blobs, map, drawn = 'src'):
    dataset = SynDataGenerator(numSamples = n_samples, numFeatures = n_features, numBlobs=n_blobs,
                               map = map, drawn = drawn,
                               transform=transforms.Compose([
                                   ToTensor()
                               ]))
    return DataLoader(dataset, batch_size=4, shuffle=True, num_workers=4)



#
# if __name__ == "__main__":
#     dataset = SynDataGenerator(numSamples = 500, numFeatures = 5,
#                                transform=transforms.Compose([
#                                    ToTensor()
#                                ]))
#     dataloader = DataLoader(dataset, batch_size=4,
#                             shuffle=True, num_workers=4)
#
#     for i_batch, sample_batched in enumerate(dataloader):
#         print(i_batch, sample_batched['features'].size(), sample_batched['label'].size())