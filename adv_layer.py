import torch
import torch.nn as nn
from torch.autograd import Function
import torch.nn.functional as F
from torch.nn import Linear, ReLU


class ReverseLayerF(Function):

    @staticmethod
    def forward(ctx, x, alpha):
        ctx.alpha = alpha
        return x.view_as(x)

    @staticmethod
    def backward(ctx, grad_output):
        output = grad_output.neg() * ctx.alpha
        return output, None

class Discriminator(nn.Module):
    def __init__(self, input_dim=256, hidden_dim=256):
        super(Discriminator, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.dis1 = nn.Linear(input_dim, hidden_dim)
        # self.bn = nn.BatchNorm1d(hidden_dim)
        self.dis2 = nn.Linear(hidden_dim, 1)


    def forward(self, x):
        x = F.relu(self.dis1(x))
        x = self.dis2(x)
        x = torch.sigmoid(x)
        return x

class ETANDiscriminator(nn.Module):
    def __init__(self, input_dim=256, hidden_dim=256):
        super(ETANDiscriminator, self).__init__()
        inputDim = input_dim
        width = hidden_dim
        self.fc1 = Linear(inputDim, width)
        self.outLayer1 = Linear(width, 1)
        self.fc2 = Linear(width, width)
        self.outLayer2 = Linear(width, 1)
        self.fc3 = Linear(width, width)
        self.outLayer3 = Linear(width, 1)
        self.fc4 = Linear(width, width)
        self.outLayer4 = Linear(width, 1)
        self.fc5 = Linear(width, width)
        self.outLayer5 = Linear(width, 1)
        self.act1 = ReLU()
        self.act2 = ReLU()
        self.act3 = ReLU()
        self.act4 = ReLU()


    def forward(self, x):
        x1 = self.fc1(x)
        output1 = self.act1(x1)
        x1 = torch.sigmoid(self.outLayer1(x1))
        x2 = self.fc2(output1)
        output2 = self.act1(x2)
        x2 = torch.sigmoid(self.outLayer1(x2))
        x3 = self.fc3(output2)
        output3 = self.act1(x3)
        x3 = torch.sigmoid(self.outLayer1(x3))
        x4 = self.fc4(output3)
        output4 = self.act1(x4)
        x4 = torch.sigmoid(self.outLayer1(x4))
        x5 = self.fc5(output4)
        x5 = torch.sigmoid(self.outLayer1(x5))
        return [x1, x2, x3, x4, x5]
